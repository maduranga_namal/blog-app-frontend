import { Component, OnInit } from '@angular/core';
import { BlogService } from '../services/blog.service';
import { Blog } from '../shared/blog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private blogservice: BlogService) { }

  blogs: Blog[];
  errMess: string;

  ngOnInit() {
    this.test();
    this.getBlogs();

  }

  test() {
    const user = sessionStorage.getItem('username');
  }

  getBlogs() {
    this.blogservice.getBlogs()
    .subscribe(blogs => this.blogs = blogs,
      errmess => this.errMess = <any>errmess);

    // console.log(this.blogs);
  }

}
