import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../shared/user';
import { AuthenticationService } from '../services/authentication.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('lform') loginFormDirective;

  loginForm: FormGroup;
  login: User;

  errMess: string;

  // username: string;
  // password: string;
  invalidLogin = false;

  constructor(private fb: FormBuilder,
              private loginservice: AuthenticationService,
              private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.login = new User();

  }

  // checkLogin(username, password) {
  //   if (this.loginservice.authenticate(username, password)
  //   ) {
  //     console.log("okay H")

  //     this.invalidLogin = false;
  //     this.router.navigate(['home']);
  //   } else {
  //     this.invalidLogin = true;

  //   }
  // }
  checkLogin(username, password) {
    (this.loginservice.authenticate(username, password).subscribe(
      data => {
        this.invalidLogin = false;
        this.router.navigate(['home']);
      },
      error => {
        this.invalidLogin = true;

      }
    )
    );

  }

  formErrors = {
    'username': '',
    'password': ''
  };

  validationMessages = {
    'username': {
      'required': 'Username is required.'
    },
    'password': {
      'required': 'Password is required.'
    }
  };

  createForm(): void {
    this.loginForm = this.fb.group({
      // email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    this.loginForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now

  }

  onValueChanged(data?: any) {
    if (!this.loginForm) { return; }
    const form = this.loginForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {

    this.login = this.loginForm.value;
    this.checkLogin(this.login.username, this.login.password);

    this.loginForm.reset({
      username: '',
      password: ''
    });
  }

}
