import { Injectable } from '@angular/core';
// import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';


import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';

import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { Blog } from '../shared/blog';


export class User{
  constructor(
    public status: string,
     ) {}

}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient: HttpClient,
              private processHTTPMsgService: ProcessHTTPMsgService) { }


  authenticate(username, password) {
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.httpClient.get<User>('http://localhost:8080/users/validateLogin', {headers}).pipe(
     map(
       userData => {
        sessionStorage.setItem('username', username);
        return userData;
       }
     )

    );
  }

  isUserLoggedIn() {
    const user = sessionStorage.getItem('username');
    return !(user === null);
  }

  logOut() {
    sessionStorage.removeItem('username');
  }

  // loginservice(): Observable<Blog[]> {
  //   return this.http.get<Blog[]>(baseURL + 'blog')
  //     .pipe(catchError(this.processHTTPMsgService.handleError));
  // }
  // authenticate(username, password) {
  //   if (username === "Maduranga" && password === "password") {
  //     sessionStorage.setItem('username', username);
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

}
