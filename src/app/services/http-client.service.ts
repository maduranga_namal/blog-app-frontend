import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class Blogger {
  constructor(
    public userId: number,
    public userName: string,
    public password: string,
    public email: string
  ) { }

}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getBlogger() {
    let userName = 'Maduranga';
    let password = 'password';

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(userName + ':' + password) });
    return this.httpClient.get<Blogger[]>('http://localhost:8080/users', { headers });
  }


  public deleteBlogger(blogger) {
    let userName = 'Maduranga';
    let password = 'password';

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(userName + ':' + password) });
    return this.httpClient.delete<Blogger>("http://localhost:8080/users" + "/" + blogger.userId, { headers });
  }

  public createBlogger(blogger) {
    let userName = 'Maduranga';
    let password = 'password';

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(userName + ':' + password) });
    return this.httpClient.post<Blogger>("http://localhost:8080/users", blogger, { headers });
  }

}
