import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { Observable } from 'rxjs';
import { Blog } from '../shared/blog';
import { catchError } from 'rxjs/operators';
import { baseURL } from '../shared/baseurl';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient,
              private processHTTPMsgService: ProcessHTTPMsgService) { }

  getBlogs() {
    let userName = 'Maduranga';
    let password = 'password';

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(userName + ':' + password) });
   // return this.http.get<Blog[]>(baseURL + 'blog', { headers} )
    return this.http.get<Blog[]>('http://localhost:8080/blog', { headers} )
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  // getBlogger() {
  //   let userName = 'Maduranga';
  //   let password = 'password';

  //   const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(userName + ':' + password) });
  //   return this.httpClient.get<Blogger[]>('http://localhost:8080/users', { headers });
  // }

}
